# 使用原因：

对于我们经常换电脑来工作的人群，在公司工作完，回家里再用U盘或网盘复制/下载我们的代码，简直是一种折磨，一个项目中断后，时间久了再去想继续的时候，你会发现：到底哪个是最新版的？！U盘满了，这个文件夹到底能不能删除？

但当你使用了git进行源码管理之后，这些问题完全就不必担心了。
当然我也是一名初学者，看了很多大神的帖子，发现很多都用到了git语句来操作，对于git小白来说，还是有点难度的。这里我介绍的方法，将使用最少的git语句`（仅仅3句）`来完成代码同步。

***
# 使用工具： 

按顺序为：git软件， 码云（gitee.com），vscode

## 1.下载，安装git软件

下载地址：https://git-scm.com/download/win

根据自己的系统选择下载即可，我的是win7 64位，我就选择的是`64-bit Git for Windows Setup`. 这个版本。下载完就一直下一步，直到安装完。
如果安装过程提示无法添加环境变量到path中，则需要将git安装目录下的cmd目录添加到系统的path变量下。

***
## 2.配置git

这一步必须用到git语句，但非常少，直接照抄就行。

打开 `git bash`，在里面照抄git语句，注意替换掉内容，注意空格。

![git bash](https://note.youdao.com/yws/res/4494/WEBRESOURCE53b74d98b6db7cdaa532df4bc9e93ced)


![照抄git语句即可](https://note.youdao.com/yws/res/4496/WEBRESOURCE15fd5b9a1fde532062e0885d4c01ba56)

```
git config --global user.name "your name" 
git config --global user.email "your email"
git config --global credential.helper store
```

其中， "`your name`" 指的是你的gitee里的昵称，"`your email`"指的是你gitee注册时的邮箱，如下图：

![your name](https://note.youdao.com/yws/res/4497/WEBRESOURCEba6f1d9340739583408308fa99201a1d)

![your email](https://note.youdao.com/yws/res/4495/BB72F1ECE25B47DA8E9D468E887F30DD)


前两句很简单，就是配置上昵称和邮箱，那第三句呢？这句也很关键，加上这句就是记住上面2个信息，以后就不用频繁输入了。

到这里，git就全部完成，以后都不用管它了。就这3句，简单吧！

***
## 3.去码云里创建仓库

顾名思义，仓库就是放代码的地方，一个项目对应一个仓库。www.gitee.com，前提是你注册登录过了。
+ 1.进入“`个人主页`”，在左侧下方，点击“`仓库`”右侧的`+号`，开始创建仓库。
  
  ![创建仓库](https://note.youdao.com/yws/res/4493/FA036D4C05334AFFA15F5D7AD7698AA3)

+ 2.里面的内容根据自己的习惯来填写即可，为了方便后期管理，最好是有规律的。最后点击下面的 `创建` 按钮即可。

![填写仓库信息，创建](https://note.youdao.com/yws/res/4499/CB060B6FD95D4EF1A31C1436965FB4A1)

+ 3.创建完会自动到仓库页面，点击仓库右侧的“克隆/下载”，点击 复制 按钮即可。这里复制的是仓库的地址，后面要用到。

![复制 仓库地址](https://note.youdao.com/yws/res/4501/1B3DE3ED0BF741D38D7611F777711D9E)

***
## 4.vscode里操作

+ 1.创建一个空白文件夹，这里我命名 “`测试`”，用vscode打开
  
  ![空白文件夹](https://note.youdao.com/yws/res/4503/2CE6917948DB4E9686E03B453D9DCD04)

+ 2.按`ctrl+shift+p`，然后输入 `git`，选择 `git:克隆`；输入我们之前复制的`仓库网址`，然后按`回车`键，选择 `测试` 文件夹作为仓库位置。这里我们可能会遇到要输入`账号密码`的，输入gitee的账号和密码即可。
  
  ![选择git 克隆](https://note.youdao.com/yws/res/4505/0396AC12DB5C4E32BBD4CD95DDFA84C0)

  ![输入仓库地址](https://note.youdao.com/yws/res/4507/E2785483AFB64BCAB3151912186FF5F1)

  ![选择测试文件夹作为存储库位置](https://note.youdao.com/yws/res/4509/035727DE13AC4917AD7C7ACF041DE787)

  ![正在克隆](https://note.youdao.com/yws/res/4511/244257C79612438F8C30461E6E73EBD8)

+ 3.打开存储库，现在就可以看到我们已经把线上的仓库内容拉取到本地了。

![打开存储库](https://note.youdao.com/yws/res/4513/82A6683D9FCB4F528C8CA0E9FFBA8DF2)

![vscode里 已经拉取到本地了](https://note.youdao.com/yws/res/4515/6D9E30FE7BB24776AEA9E0A4F9B7C864)

![本地文件夹，注意这个git文件夹是隐藏的，不可删除](https://note.youdao.com/yws/res/4517/7D339833078E4DF6867BAB8EF5099700)

***
## 测试：修改本地文件，然后提交到码云

+ 1.本地创建完文件后，vsCode里我们就可以看到这些变化，新增的文件时绿色的，后面有个 `U` 字母，左侧图标上会有数字显示
  
  ![注意这些变化](https://note.youdao.com/yws/res/4519/C548FC7C88F24395829433525DA5E11A)

+  2.点击左侧图标，然后点击更改旁边的`+`号，暂存所有的更改

![暂存所有的更改](https://note.youdao.com/yws/res/4521/20FD6666F3D84A63B1B3FB4B65F83A3D)

+ 3.在上面输入文字，这里的文字要好好写，后面你会发现是很有必要的，写好后点击 `ctrl+enter`，就提交了


![提交](https://note.youdao.com/yws/res/4523/9892C1CE8AC34112899C1F844E4AF325)

```
注：上面第2 3步也可以只操作第三步即可
```
+ 4.点击 `推送`，把这些更改push到码云仓库里
  
  ![push推送](https://note.youdao.com/yws/res/4525/DCD7881ED88047698D6A2410120EB9DB)

+  5.我们进码云里看下，动态和仓库里都可以看到我们最新的记录和文件了


![仓库里也显示了，有具体的时间，还有刚才的文字备注](https://note.youdao.com/yws/res/4527/98A44E26C8124DC089B8909481231008)

***
## 结论

到此就完全学会了，超级简单吧！~~

