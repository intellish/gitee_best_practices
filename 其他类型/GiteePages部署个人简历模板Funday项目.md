Gitee Pages部署个人简历模板Funday项目
========


前言
--------

在码云发布Funday之后，有好多的朋友私信或者留言说不知道怎么使用，不知道怎么部署，所以就写一篇文章来详细介绍下使用细节步骤吧。


Fork项目
--------

在浏览器中打开Funday的主页，[https://gitee.com/xiaodan_yu/resume.io](https://gitee.com/xiaodan_yu/resume.io)。

点击右上角的Fork，在弹出的对话框中，选择自己的账号名，点击确定。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0607/122549_44b58842_1710039.png "fork")

稍等一会，就会在跳转到自己的项目页面。


开通Gitee Pages服务
--------

在项目主页点击`服务`-`Gitee Pages`,并在pages页面中选择部署分支为`master`,确保`强制使用HTTPS`是选中状态，点击`启动`。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0607/132018_3b8beb01_1710039.png "gitee pages.png")

稍等一会之后，会出现gitee pages开通成功的通知，可以通过显示的地址访问到生成的pages。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0607/132746_1f9d6b5b_1710039.png "gitee pages open.png")

[https://xiaodan_yu.gitee.io/resume.io](https://xiaodan_yu.gitee.io/resume.io)


Git Clone项目到本地
--------

将Fork后的项目clone到本地

```
$ git clone https://gitee.com/xiaodan_yu/resume.io.git
Cloning into 'resume.io'...
remote: Enumerating objects: 158, done.
remote: Counting objects: 100% (158/158), done.
remote: Compressing objects: 100% (148/148), done.
remote: Total 158 (delta 61), reused 11 (delta 3)
Receiving objects: 100% (158/158), 5.56 MiB | 621.00 KiB/s, done.
Resolving deltas: 100% (61/61), done.
```


修改个人信息
--------

1. 修改`_config.yml`文件中的内容

```
# 个人名称或昵称
name: xiaoxiao
# 页面个人头像信息中地址展示信息
location: 大连
# 页面个人头像信息中公司展示信息
company: IBM
# 页面个人头像信息中职位展示信息
position: Java开发工程师
# 页面个人头像信息中GITHUB展示信息
github: https://github.com/XXXX
# 页面个人头像信息中Facebook展示信息
facebook: https://www.facebook.com/XXXX
# 页面个人头像信息中电话展示信息
phone: 1580424XXXX
# 页面个人头像信息中EMAIL展示信息
email: xxxx@xxx.com

#本项目的baseurl
baseurl: "/resume.io"
```

2. 修改个人头像信息

	修改 `_config.yml` 文件中内容

3. 修改基本信息
 
	修改 `_includes/resumer_01-basic.html` 文件中内容

4. 修改职业技能

    修改 `_includes/resumer_02-profetional.html` 文件中内容

5. 修改教育经历

    修改 `_includes/resumer_03-education.html` 文件中内容

6. 修改工作经历

    修改 `_includes/resumer_04-experience.html` 文件中内容

7. 修改获得证书

	修改 `_includes/resumer_05-certification.html` 文件中内容

8. 修改个人作品

	修改 `_includes/resumer_06-personal_project.html` 文件中内容


本地测试
--------

安装Jekyll [https://jekyllrb.com/docs/installation/](https://jekyllrb.com/docs/installation/)
```
jekyll s
```

推送到远程
--------

```
git add .
git commit -a -m "do some change"
git push origin master
```

查看项目运行页面
--------

[https://xiaodan_yu.gitee.io/resume.io](https://xiaodan_yu.gitee.io/resume.io)


其他
--------
更多信息可以参考Funday项目的README文件[https://gitee.com/xiaodan_yu/resume.io](https://gitee.com/xiaodan_yu/resume.io)