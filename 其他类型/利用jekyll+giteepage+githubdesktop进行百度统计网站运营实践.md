# 利用jekyll+giteepage+githubdesktop进行seo网站运营实际

## 选择github里面的jekyll主题 Basically Basic Jekyll Theme

# [Basically Basic Jekyll Theme（需要修改的模板）][1]
[1]: https://mmistakes.github.io/jekyll-theme-basically-basic/

### 选用理由

- 简洁的响应式设计，可更换六个可自定义的皮肤[six customizable skins](#skin)
- 课程Vitæ/ Resume布局由JSON数据提供支持[JSON data](http://registry.jsonresume.org/)
- 关于页面布局
- 由Algolia或Lunr提供的全站搜索[Algolia](https://www.algolia.com/) or [Lunr](https://lunrjs.com/).
- Disqus评论和Google Analytics支持
- 通过Jekyll SEO Tag的 SEO最佳实践 [Jekyll SEO Tag](https://github.com/jekyll/jekyll-seo-tag/)


# [黄杰琪的jekyll网站（修改后）][1]
[1]: https://huangjieqi.gitee.io/
## 访问：
1. Gitee Pages 提供的域名解析[https://huangjieqi.gitee.io/](https://huangjieqi.gitee.io/)   
2. 通过Gitee Pages Pro 服务提供的自定义域名解析[https://www.nfuedu.cn/](https://www.nfuedu.cn/)
## 学习：
### [实践入门手册](https://gitee.com/huangjieqi/huangjieqi/blob/gh-pages/README.md)
### [jekyll官网](https://www.jekyll.com.cn/)

##  实践内容
使用Jekyll之jekyll-theme-basically-basic主题模板在Gitee上架示范站[huangjieqi.gitee.io](https://gitee.com/huangjieqi/huangjieqi)
  * 特别注意此次架站示范,使用的是gh-pages分支，而非master分支
  * 程式码[提交记录](https://gitee.com/huangjieqi/huangjieqi/commits/gh-pages)
  * 程式码[仓库网路图](https://gitee.com/huangjieqi/huangjieqi/graph/gh-pages)
#### 网站设计
- [实践jekyll网站设计中编写摘要隐藏文章部分内容](https://huangjieqi.gitee.io/huangjieqi/%E7%BD%91%E7%AB%99%E8%AE%BE%E8%AE%A1/2019/06/25/Website-Design_more.html)
- [实践jekyll导航栏的分类文章设置](https://huangjieqi.gitee.io/huangjieqi/%E7%BD%91%E7%AB%99%E8%AE%BE%E8%AE%A1/2019/06/24/Website-Design_nav.html)
- [实践jekyll插件实现文章分页显示](https://huangjieqi.gitee.io/huangjieqi/%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0/2019/06/27/Study-Notes_jekyll_Pagination.html)

  
#### 平面设计
- [实践jekyll网站的字体更换](https://huangjieqi.gitee.io/huangjieqi/%E5%B9%B3%E9%9D%A2%E8%AE%BE%E8%AE%A1/2019/06/21/Plane-Design_chenxianziti.html)
- [实践jekyll网站的页面重构，标题隐藏](https://huangjieqi.gitee.io/huangjieqi/%E5%B9%B3%E9%9D%A2%E8%AE%BE%E8%AE%A1/2019/06/20/Plane-Design_yinchangshu.html)
- [实践jekyll网站的svg动画实验](https://huangjieqi.gitee.io/huangjieqi/tag/svg%E5%88%B6%E4%BD%9C/)



#### 百度统计网站运营在通过添加[百度统计](https://gitee.com/huangjieqi/huangjieqi/commits/6648481e372d7d669eb39b832ee55e0d8e7d4478/_includes/head.html)

![百度统计运营报告](https://images.gitee.com/uploads/images/2019/0627/224729_7745fabe_2232418.png "")
![百度统计运营报告](https://images.gitee.com/uploads/images/2019/0627/224809_311783a1_2232418.png "")
![百度统计运营报告](https://images.gitee.com/uploads/images/2019/0627/224813_355c3638_2232418.png "")

### 总结
- 通过Gitee Pages Pro 服务提供的自定义域名以及自动部署，减少了网站的部署时间，增加网站运营的可行性
- 利用Gitee Pages 提供的jekyll网站部署，达到了一个免费的网站运营模式，只需进行网站的前端修改和数据分析即可达到网站运营的效果
- 通过gitee平台创建了一个开源的平台，在前端技术方面有了更多的参考，并思考出更多的可能性。