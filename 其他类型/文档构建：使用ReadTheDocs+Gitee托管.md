作者：zhyantao

在开发过程中，需要不断的进行归纳总结，常见的做法就是通过博文来进行记录，比如CSDN和博客园。但是随着文章数量的增多，这些网站管理起来就显得不是那么方便了。因此，一个较为稳妥的方式就是下面的介绍了。


使用Gitee+Read the Docs托管文档 
===============================

[![Documentation Status](https://readthedocs.org/projects/gitee-readthedocs/badge/?version=latest)](https://gitee-readthedocs.readthedocs.io/zh_CN/latest/?badge=latest)

## 一、文档撰写前提

环境部署:

```
> git clone https://gitee.com/zhyantao/readthedocs.git
> pip install sphinx recommonmark sphinx-autobuild sphinx_rtd_theme -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host=mirrors.aliyun.com
```

## 二、撰写博文并发表

1. 把需要发表的文档放在 *source/docs* 文件夹中（写作格式可以是markdown或者reStructuredText）
2. CMD中输入 `make html` 回车，打开 *build/html/index.html* 预览效果
3. 提交代码（博文）到 Gitee 仓库 [[git简易指南](http://www.bootcss.com/p/git-guide/)]
4. 在 [Read the Docs](https://readthedocs.org/)中导入 Gitee 项目
5. 导入成功后，点击 View the documentation 查看最终效果 [[示例](https://gitee-readthedocs.readthedocs.io/zh_CN/latest/docs/preface.html)]

**Warning**

除了 *demo-readthedocs/source/docs* 下的文件和 *source/index.rst* 可以修改外，其他位置的文件不要修改，否则可能会引起错误。另外，**务必在 source/index.rst 文件中添加你的博文的路径**